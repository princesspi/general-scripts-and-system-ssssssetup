<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="description" content="Check if your password has been compromised in a hacked leak">
<title>Check Integrity of Passwords</title>
</head>
<body>
<h1>Has your password been compromised in a leak?</h1>
<form action="sha.php" method="post">
<input type="password" name="password"> <input type="submit" value="check">
</form>
<p>
<?php
if($_POST['password']) {
$sha1 = strtoupper(sha1($_POST['password']));
$return = shell_exec("sh test.sh $sha1");
if(strlen($return) > 1) { echo "This password has been compromised, please pick another"; }
else { echo "This password is good! Congratulations!"; }
}
?>
</p>
</body>
</html>
