#!/bin/bash
[ "root" != "$USER" ] && exec sudo $0 "$@"
KEY=''
mkdir /home/$1/.ssh
echo $KEY >> /home/$1/.ssh/authorized_keys
chmod 700 /home/$1/.ssh
chmod 600 /home/$1/.ssh/authorized_keys
chown -R $1:$1 /home/$1/.ssh
